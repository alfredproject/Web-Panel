export const navItems = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-energy',
  },
  {
    name: 'Boards',
    url: '/boards',
    icon: 'icon-equalizer',
  },
  // {
  //   name: 'Settings',
  //   url: '/settings',
  //   icon: 'icon-settings',
  // },
  {
    name: 'Users',
    url: '/users',
    icon: 'icon-user'
  },
];
