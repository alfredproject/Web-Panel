import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse} from '@angular/common/http';
import {Observable} from "rxjs";
import { Router } from '@angular/router';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private router: Router){

  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authReq = req.clone({ headers: req.headers.set('Authorization', localStorage.getItem('token') || '')});
    return next.handle(authReq).catch((err: any) => {
    	if (err instanceof HttpErrorResponse)
    		if (err.status == 403)
				this.router.navigateByUrl('login');

		return Observable.throw(err);
    });
  }
}