import { Component, OnInit, ElementRef, AfterContentInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { BoardsService } from '../boards.service';
import { PluginsService } from '../plugins.service';
import { Board } from '../board';
import { Plugin } from '../plugin';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-board-detail',
  templateUrl: './board-detail.component.html',
  styleUrls: ['./board-detail.component.scss']
})
export class BoardDetailComponent implements OnInit, AfterContentInit {
	boardForm: FormGroup;

	public removeModal;
	public commandSuccess: BsModalRef;
	public modalRef: BsModalRef;
	public commandError: BsModalRef;
	@ViewChild('commandSuccess') commandSuccessRef : ElementRef;
	@ViewChild('commandError') commandErrorRef : ElementRef;
	_sampleBoards = [
		{'id': 1, 'name': 'Led', 'description': 'Turn on/off a led', 'plugin': 'Light', 'connected': true},
		{'id': 2, 'name': 'Led2', 'description': 'Turn on/off a led', 'plugin': 'Light', 'connected': true},
		{'id': 3, 'name': 'Blind', 'description': 'Open or close the blind', 'plugin': 'DC Motor', 'connected': true},
		{'id': 4, 'name': 'Blind2', 'description': 'Open or close the blind', 'plugin': 'DC Motor', 'connected': false}

	];
	board : Board;
	plugins: Plugin[];

  constructor(private route: ActivatedRoute, private boardService: BoardsService, private pluginService: PluginsService, private fb: FormBuilder, private router: Router, private modalService: BsModalService) {
  	this.boardForm = fb.group({
  		'name': '',
  		'description': '',
  		'plugin': null
  	});
  }

  ngOnInit() {
  	this.getPlugins();
  	this.getBoard();
  }

  ngAfterContentInit() {
  }

  getBoard() : void {
  	let id = this.route.snapshot.paramMap.get('id');
  	this.boardService.getBoardById(id)
  		.subscribe((res) => {
  			this.board = res;
  			this.boardForm = this.fb.group({
		  		'name': res.name,
		  		'description': res.description,
		  		'plugin': res.plugin
		  	});
  		});
  }

  getPlugins() {
  	this.pluginService.getPlugins()
  		.subscribe((res) => {
  			this.plugins = res;
  		});
  }

  onSubmit(value) {
  	this.board.name = value.name;
  	this.board.description = value.description;
  	this.board.plugin = value.plugin;
  	this.updateBoard();
  }

  onReset() {
  	this.boardForm.reset({
		  		'name': this.board.name,
		  		'description': this.board.description,
		  		'plugin': this.board.plugin
		  	});
  }

  compare(): boolean {
  	return this.board != undefined && (this.boardForm.value.name == this.board.name && 
  		this.boardForm.value.description == this.board.description &&
  		this.boardForm.value.plugin == this.board.plugin);
  }

  updateBoard() {
  	this.boardService.updateBoard(this.board)
  		.subscribe((res) => {
  			console.log(res);
  		});
  }

  removeBoard() {
  	this.boardService.deleteBoard(this.route.snapshot.paramMap.get('id'))
  		.subscribe((res) => {
  			this.router.navigateByUrl('boards');
  		});
  }

  getBoardPlugin() {
  	if (this.board && this.plugins) {
  		return this.plugins.find(plugin => plugin['_id'] == this.board.plugin)['commands'];
  	}

  	return [];
  }

  callCommand(command) {
  	this.boardService.callCommand(this.board['_id'], command)
  		.subscribe(res => {
			this.modalRef = this.modalService.show(this.commandSuccessRef, Object.assign({}, { class: 'modal-dialog modal-success modal-sm' }));
  		}, err => {
			this.modalRef = this.modalService.show(this.commandErrorRef, Object.assign({}, { class: 'modal-dialog modal-warning modal-sm' }));
  		});
  }

}
