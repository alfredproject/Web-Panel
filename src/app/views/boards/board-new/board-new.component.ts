import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BoardsService } from '../boards.service';
import { PluginsService } from '../plugins.service';
import { Router } from '@angular/router';
import { Board } from '../board';
import { Plugin } from '../plugin';

@Component({
  selector: 'app-board-new',
  templateUrl: './board-new.component.html',
  styleUrls: ['./board-new.component.scss']
})
export class BoardNewComponent implements OnInit {
	newBoard: Board;
  boardForm: FormGroup;
	plugins : Plugin[];

  constructor(private boardService: BoardsService, private pluginService: PluginsService, private fb: FormBuilder, private router: Router) {
    this.boardForm = fb.group({
      'name': '',
      'description': '',
      'plugin': null
    });
  }

  ngOnInit() {
  	this.getPlugins();
  }

  getPlugins() {
    this.pluginService.getPlugins()
      .subscribe((res) => {
        this.plugins = res;
      });
  }

  onSubmit(value) {
    this.newBoard = new Board();
    this.newBoard.name = value.name;
    this.newBoard.description = value.description;
    this.newBoard.plugin = value.plugin;
    this.newBoard.active = false;
    this.boardService.createBoard(this.newBoard)
      .subscribe((res) => {
        console.log(res);
        this.router.navigateByUrl('boards');
      });
  }

  compare(): boolean {
    return /^\s*$/.test(this.boardForm.value.name) ||
      /^\s*$/.test(this.boardForm.value.description) ||
      (this.boardForm.value.plugin != null && /^\s*$/.test(this.boardForm.value.plugin));
  }

}
