import * as moment from 'moment';
import {Plugin} from './plugin';

export class Board {
	id;
	name : string;
	description : string;
	plugin : any;
	active : boolean;
	connected : boolean;
	startTime : moment.Moment;

	constructor (id?, name? : string, description? : string, plugin? : any, active? : boolean, connected? : boolean, startTime? : Date) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.plugin = plugin;
		this.startTime = startTime == undefined ? moment() : moment(startTime);
		this.connected = connected;
		this.active = this.connected == true ? active : false;
	}

};

export default Board;