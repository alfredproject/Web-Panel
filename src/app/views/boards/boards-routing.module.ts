import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BoardsComponent } from './boards.component';
import { BoardDetailComponent } from './board-detail/board-detail.component';
import { BoardNewComponent } from './board-new/board-new.component';

const routes: Routes = [
  {
    path: '',
    component: BoardsComponent,
    data: {
      title: 'Boards'
    },
  },
  {
    path: '',
    data: {
      title: 'Board'
    },
    children: [
      {
        path: 'new',
        component: BoardNewComponent,
        data: {
          title: 'New'
        }
      },
      {
        path: ':id',
        component: BoardDetailComponent,
        data: {
          title: 'detail'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BoardsRoutingModule {}
