import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { BoardsService } from './boards.service';
import { PluginsService } from './plugins.service';
import { Board } from './board';
import { Plugin } from './plugin';

@Component({
	selector: 'app-boards',
	templateUrl: './boards.component.html',
	styleUrls: ['./boards.component.scss']
})
export class BoardsComponent implements OnInit {
	_sampleBoards = [
		{'id': 1, 'name': 'Led', 'description': 'Turn on/off a led', 'plugin': 'Light', 'connected': true},
		{'id': 2, 'name': 'Led2', 'description': 'Turn on/off a led', 'plugin': 'Light', 'connected': true},
		{'id': 3, 'name': 'Blind', 'description': 'Open or close the blind', 'plugin': 'DC Motor', 'connected': true},
		{'id': 4, 'name': 'Blind2', 'description': 'Open or close the blind', 'plugin': 'DC Motor', 'connected': false}

	];
	boards : Board[] = [];
	timeFrom : moment.Moment;
	now_ : moment.Moment;
	constructor(private boardService: BoardsService, private pluginsService: PluginsService) { }

	ngOnInit() {
		this.getBoards();
		// for (let i = 0; i < 4; i++) {
		// 	let board = this._sampleBoards[i];
		// 	let active = Math.floor(Math.random() * (1 - 0 + 1)) + 0;
		// 	this.boards.push(new Board(board.id, board.name, board.description, new Plugin(board.plugin, "Juan Manez", "1.0"), !!active, board.connected));
		// }

		this.now();
	}

	getBoards() {
		this.boardService.getBoards()
			.subscribe((res) => {
				this.boards = res;
				this.boardService.getConnectedBoards()
					.subscribe((res) => {
						for (let board of this.boards) {
							board.startTime = moment();
							board.connected = false;

							this.pluginsService.getPluginById(board.plugin)
								.subscribe((res) => {
									board.plugin = res;
								});

							for (let connectedBoard of res) {
								console.log(connectedBoard.name + " " + board.name)
								if (board.name === connectedBoard.name) {
									board.startTime = moment(connectedBoard.startTime);
									board.connected = true;
								}
							}
						}
						console.log(this.boards);
					});
			});
	}

	now()  {
		this.now_ = moment();
		setTimeout(() => {
  			this.now();
		}, 10000);
	}

}
