import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { ModalModule } from 'ngx-bootstrap/modal';

import { MomentModule } from 'ngx-moment';
import { BoardsComponent } from './boards.component';
import { BoardsRoutingModule } from './boards-routing.module';
import { BoardDetailComponent } from './board-detail/board-detail.component';
import { BoardNewComponent } from './board-new/board-new.component';
import { BoardsService } from './boards.service';
import { PluginsService } from './plugins.service';
import { AuthInterceptor } from '../../interceptors/auth.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BoardsRoutingModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    MomentModule,
    ModalModule.forRoot(),
    HttpClientModule
  ],
  declarations: [
  	BoardsComponent,
  	BoardDetailComponent,
  	BoardNewComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    BoardsService,
    PluginsService
  ]
})
export class BoardsModule { }
