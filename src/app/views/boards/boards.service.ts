import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import Board from './board';

@Injectable({
  providedIn: 'root'
})
export class BoardsService {
	boards_url = 'http://192.168.1.50:80/api/boards';

  constructor(private http: HttpClient) { }

  getBoards(): Observable<Board[]> {
  	return this.http.get(this.boards_url)
  		.map(res => {
  			return res["data"] as Board[];
  		});
  }

  getConnectedBoards(): Observable<any> {
  	return this.http.get(this.boards_url + "/connected")
  		.map(res => {
  			return res["data"];
  		});
  }

  getBoardById(id: string): Observable<Board> {
  	return this.http.get(this.boards_url + "/" + id)
  		.map(res => {
  			return res['data'];
  		})
  }

  updateBoard(board): Observable<Board> {
  	return this.http.put(this.boards_url, {'boardData': board})
  		.map(res => {
  			return res["data"];
  		});
  }

  createBoard(board): Observable<Board> {
  	return this.http.post(this.boards_url, {'boardData': board})
  		.map(res => {
  			return res["data"];
  		});
  }

  deleteBoard(boardId): Observable<any> {
  	return this.http.delete(this.boards_url + "/" + boardId);
  }

  callCommand(boardId, command, data?): Observable<any> {
  	console.log(command.type === 'get')
  	if (command.type === 'get')
  		return this.http.get(this.boards_url + "/" + boardId + "/" + command.path);
  	else if (command.type === 'post')
  		return this.http.get(this.boards_url + "/" + boardId + "/" + command.path, data);
  	else if (command.type === 'put')
  		return this.http.put(this.boards_url + "/" + boardId + "/" + command.path, data);
  	else if (command.type === 'delete')
  		return this.http.delete(this.boards_url + "/" + boardId + "/" + command.path);

  }
}
