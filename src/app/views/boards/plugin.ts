export class Plugin {
	name : string;
	author : string;
	version : string;

	constructor(name : string, author : string, version : string) {
		this.name = name;
		this.author = author;
		this.version = version;
	}
};

export default Plugin;