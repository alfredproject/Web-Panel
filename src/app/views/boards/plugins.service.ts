import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import Plugin from './plugin';

@Injectable({
  providedIn: 'root'
})
export class PluginsService {
	plugins_url = 'http://192.168.1.50:80/api/plugins';

  constructor(private http: HttpClient) { }

  getPlugins(): Observable<Plugin[]> {
  	return this.http.get(this.plugins_url)
  		.map(res => {
  			return res["data"] as Plugin[];
  		});
  }

  getPluginById(id: string): Observable<Plugin> {
    return this.http.get(this.plugins_url + "/" + id)
      .map(res => {
        return res["data"] as Plugin;
      })
  }

}
