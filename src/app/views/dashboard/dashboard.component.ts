import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';

import { BoardsService } from '../boards/boards.service';
import { PluginsService } from '../boards/plugins.service';

import Board from '../boards/board';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  boards: Board[];
    now_ : moment.Moment;
    constructor(private boardService: BoardsService, private pluginService: PluginsService) { }

    ngOnInit() {
      this.now();
      this.getBoards();
    }

    now()  {
      this.now_ = moment();
      setTimeout(() => {
          this.now();
      }, 10000);
    }

    getBoards() {
    this.boardService.getBoards()
      .subscribe((res) => {
        this.boards = res;
        this.boardService.getConnectedBoards()
          .subscribe((res) => {
            for (let board of this.boards) {
              board.startTime = moment();
              board.connected = false;

              this.pluginService.getPluginById(board.plugin)
                .subscribe((res) => {
                  board.plugin = res;
                });

              for (let connectedBoard of res) {
                if (board.name === connectedBoard.name) {
                  board.startTime = moment(connectedBoard.startTime);
                  board.connected = true;
                }
              }
            }
          });
      });
  }
}
