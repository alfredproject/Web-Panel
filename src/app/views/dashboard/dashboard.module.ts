import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { MomentModule } from 'ngx-moment';
import { LogTerminalComponent } from './log-terminal/log-terminal.component';

import { BoardsService } from '../boards/boards.service';
import { PluginsService } from '../boards/plugins.service';

@NgModule({
  imports: [
  	CommonModule,
    FormsModule,
    DashboardRoutingModule,
    // ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    MomentModule
  ],
  declarations: [ DashboardComponent, LogTerminalComponent ],
  providers: [
  	BoardsService,
  	PluginsService
  ]
})
export class DashboardModule { }
