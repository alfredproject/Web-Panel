import { Component, OnInit, ElementRef, AfterViewInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { WebSocketSubject } from 'rxjs/observable/dom/WebSocketSubject';

export class Message {
    constructor(
        public sender: string,
        public content: string,
        public isBroadcast = false,
    ) { }
}

@Component({
  selector: 'app-log-terminal',
  templateUrl: './log-terminal.component.html',
  styleUrls: ['./log-terminal.component.scss']
})
export class LogTerminalComponent implements OnInit, AfterViewInit {
	@ViewChild('logger') logger : ElementRef;

	private socket$: WebSocketSubject<any>;

  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
  	this.socket$ = new WebSocketSubject('ws://192.168.1.50:80');
  	this.socket$
  		.subscribe((message) => {
  			this.addEntry(message);
  		},
  		(err) => console.error(err),
            () => console.warn('Completed!'));
  }

  addEntry(log) {
  	let shouldScroll = this.logger.nativeElement.scrollHeight - this.logger.nativeElement.scrollTop <= this.logger.nativeElement.offsetHeight;
  	
  	this.logger.nativeElement.innerHTML += `<span class="text-success">&#36;</span> ` + log.date + ` &gt; ` + log.content + `<br/>`

  	if (shouldScroll)
  		this.logger.nativeElement.scrollTop = this.logger.nativeElement.scrollHeight;
  }

}
