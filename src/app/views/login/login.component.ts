import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LoginService } from './login.service';
import { Router } from '@angular/router'
import User from '../../models/user.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
	loginForm: FormGroup;

	constructor(private fb: FormBuilder, private loginService: LoginService, private router: Router) {
		this.loginForm = fb.group({
			'username': '',
			'password': ''
		});
	}

	ngOnInit() {

	}

	onSubmit(value) {
		let user = new User(value.username, value.password);
		this.loginService.login(user)
			.subscribe((res) => {
				localStorage.setItem('token', res.data);
				this.router.navigateByUrl('dashboard');
			}, (err) => {
				this.loginForm.reset();
			});
	}
}
