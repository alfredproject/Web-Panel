import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Response } from '@angular/http';

import User from '../../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
	login_url = 'http://192.168.1.50:80/api/login';

  constructor(private http: HttpClient) { }

  login(user: User): Observable<any> {
  	return this.http.post(this.login_url, {'userData': user});
  }
}
