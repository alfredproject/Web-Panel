import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
	_show = false;
	_pwdType = 'password'

  constructor() { }

  ngOnInit() {
  }

  toggleShowPwd() {
  	this._show = !this._show
 	this._pwdType = this._show ? 'text' : 'password'
  }

}
