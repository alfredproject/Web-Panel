import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersService } from '../users.service';
import { User } from '../user';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
	public removeModal;
	user : User;
	userForm: FormGroup;

  constructor(private route: ActivatedRoute, private userService: UsersService, private fb: FormBuilder, private router: Router) {
  	this.userForm = fb.group({
  		'username': '',
  		'password': ''
  	});
  }

  ngOnInit() {
  	this.getUser();
  }

  getUser()  {
  	let id = this.route.snapshot.paramMap.get('id');
  	this.userService.getUserById(id)
  		.subscribe((res) => {
  			this.user = res;
  			this.userForm = this.fb.group({
		  		'username': this.user.username,
		  		'password': this.user.password
		  	});
  		});
  }

  onSubmit(value) {
  	this.user.username = value.username;
  	this.user.password = value.password;
  	this.updateUser();
  }

  onReset() {
  	this.userForm.reset({
		  		'username': this.user.username,
		  		'password': this.user.password
		  	});
  }

  compare(): boolean {
  	return this.user != undefined && (this.userForm.value.username == this.user.username && 
  		this.userForm.value.password == this.user.password);
  }

  updateUser() {
  	this.userService.updateUser(this.user)
  		.subscribe((res) => {
  			console.log(res);
  		});
  }

  removeUser() {
  	this.userService.deleteUser(this.route.snapshot.paramMap.get('id'))
  		.subscribe((res) => {
  			this.router.navigateByUrl('users');
  		});
  }

}
