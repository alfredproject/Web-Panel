import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersService } from '../users.service';
import { User } from '../user';

@Component({
  selector: 'app-user-new',
  templateUrl: './user-new.component.html',
  styleUrls: ['./user-new.component.scss']
})
export class UserNewComponent implements OnInit {
	newUser : User;
	userForm: FormGroup;

  constructor(private userService: UsersService, private router: Router, private fb: FormBuilder) {
  	this.userForm = fb.group({
  		'username': '',
  		'password': ''
  	});
  }

  ngOnInit() {
  	
  }

  onSubmit(value) {
  	this.userService.createUser(value)
  		.subscribe((res) => {
  			this.router.navigateByUrl('users');
  		});
  }

  compare(): boolean {
    return /^\s*$/.test(this.userForm.value.username) ||
      /^\s*$/.test(this.userForm.value.password);
  }

}
