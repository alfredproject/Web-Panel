export class User {
	username : string;
	role : string;
	password: string;

	constructor(username : string, role : string) {
		this.username = username;
		this.role = role;
	}
}

export default User;