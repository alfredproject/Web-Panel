import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { UsersComponent } from './users.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserNewComponent } from './user-new/user-new.component';

const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
    data: {
      title: 'Users'
    }
  },
  {
    path: '',
    data: {
      title: 'User'
    },
    children: [
      {
        path: 'new',
        component: UserNewComponent,
        data: {
          title: 'New'
        }
      },
      {
        path: ':id',
        component: UserDetailComponent,
        data: {
          title: 'detail'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {}
