import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';
import User from './user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
	users: User[];
  constructor(private userService: UsersService) { }

  ngOnInit() {
  	this.getUsers();
  }

  getUsers() {
  	this.userService.getUsers()
  		.subscribe((res) => {
  			this.users = res;
  		});
  }

}
