import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { ModalModule } from 'ngx-bootstrap/modal';

import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users-routing.module';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserNewComponent } from './user-new/user-new.component';
import { UsersService } from './users.service';
import { AuthInterceptor } from '../../interceptors/auth.service';

@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot()
  ],
  declarations: [UsersComponent, UserDetailComponent, UserNewComponent],
  providers: [
  	  {
	    provide: HTTP_INTERCEPTORS,
	    useClass: AuthInterceptor,
	    multi: true
	  },
  	UsersService
  ]
})
export class UsersModule { }
