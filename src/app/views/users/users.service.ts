import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import User from './user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
	users_url = 'http://192.168.1.50:80/api/users';

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {
  	return this.http.get(this.users_url)
  		.map(res => {
  			return res["data"] as User[];
  		});
  }

  getUserById(id: string): Observable<User> {
  	return this.http.get(this.users_url + "/" + id)
  		.map(res => {
  			return res['data'];
  		})
  }

  updateUser(user): Observable<User> {
  	return this.http.put(this.users_url, {'userData': user})
  		.map(res => {
  			return res["data"];
  		});
  }

  createUser(user): Observable<User> {
  	return this.http.post(this.users_url, {'userData': user})
  		.map(res => {
  			return res["data"];
  		});
  }

  deleteUser(userId): Observable<any> {
    return this.http.delete(this.users_url + '/' + userId);
  }
}
